# React Fundamentals
Review React fundamental

## Content
* react-fundamentals
    * Create React app
    * Folder structure
    * Component Types
    * Function Component
    * Class Component
    * Props
    * State, setState, prevState
    * Destructing props and state
    * Event Handling in functional components and class components
    * Methods as props
    * Condition rendering
    * List rendering
    * List and Key
    * Index as Key Anti-pattern
    * Styling and css
    * Form Handling
    * Component Lifecycle Methods
    * Component Mounting Life Cycle Methods
    * Component Updating Life Cycle Methods
    * Fragments
    * Pure Components - pure component will be rendered when the relevant state value changing.
    * Memo Components - memo component will be rendered when the props value changing.
    * Refs and Forward Refs
    * Portals
    

* react-http
* react-routing
* react-redux
* react-utitities