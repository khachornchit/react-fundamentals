import React, { Component } from "react";
import "./App.css";
import Greet from "./components/functionComponents/Greet";
import Message from "./components/classComponents/Message";
import Visitor from "./components/classComponents/Visitor";
import Count from "./components/classComponents/Count";
import FunctionClick from "./components/functionComponents/FunctionClick";
import ClassClick from "./components/classComponents/ClassClick";
import BindEventHandler from "./components/classComponents/BindEventHandler";
import ParentComponent from "./components/parent/ParentComponent";
import CustomerList from "./components/functionComponents/CustomerList";
import Stylesheet from "./components/stylesheet/Stylesheet";
import StylesheetInline from "./components/stylesheet/StylesheetInline";
import StyleSheetModule from "./components/stylesheet/StyleSheetModule";
import Form from "./components/classComponents/Form";
import Table from "./components/fragments/Table";
import RefsDemo from "./components/refs/RefsDemo";
import InputParent from "./components/refs/InputParent";
import FRInputParent from "./components/refs/forwarding/FRInputParent";
import PortalDemo from "./components/portal/PortalDemo";
import HighOrderCounter from "./components/higherOrder/HighOrderCounter";
import RenderProps from "./components/renderProps/RenderProps";
// import AppHero from "./components/errorBoudary/AppHero";
// import LifeCycleMountParent from "./components/lifeCycle/LifeCycleMountParent";
// import LifeCycleUpdate from "./components/lifeCycle/LifeCycleUpdate";
// import PureParentComponent from "./components/pureComponents/PureComponentParent";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Visitor></Visitor>
        <HighOrderCounter />
        <RenderProps />
        {/* <AppHero /> */}
        <PortalDemo />
        <FRInputParent />
        <RefsDemo />
        <InputParent />
        {/* <PureParentComponent /> */}
        {/* <LifeCycleMountParent /> */}
        {/* <LifeCycleUpdate /> */}
        <Table />
        <Form />
        <CustomerList />
        <ParentComponent />
        <BindEventHandler />
        <FunctionClick />
        <ClassClick />
        <Stylesheet class="primary" />
        <StylesheetInline />
        <StyleSheetModule />
        <Count />

        <Greet name="Lisa" title="Popupar blackpink girl">
          A member singer of blackpink girl group
        </Greet>
        <Greet name="Ariana Grance" title="Popular singer">
          A popular singer girl.
        </Greet>

        <Message name="Lisa">Message the children props</Message>
        <Message name="Ariana">Message the children props</Message>
        <Message name="Blackpink">Message the children props</Message>
        <Message name="Ariana">Message the children props</Message>
        <Message name="Maroon5">Message the children props</Message>
      </div>
    );
  }
}

export default App;
