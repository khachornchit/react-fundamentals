import React, { Component } from "react";

class Count extends Component {
  constructor() {
    super();
    this.state = {
      count: 0
    };
  }

  increase() {
    this.setState(
      {
        count: this.state.count + 1
      },
      () =>
        console.log(
          "Callback function after setState count = ",
          this.state.count
        )
    );
  }

  increasePrev() {
    this.setState(
      prevState => ({
        count: prevState.count + 1
      }),
      () =>
        console.log(
          "Callback function after setState count = ",
          this.state.count
        )
    );
  }

  increaseFive() {
    this.increasePrev();
    this.increasePrev();
    this.increasePrev();
    this.increasePrev();
    this.increasePrev();
  }

  render() {
    return (
      <div className="Component-margin">
        <strong>Count : {this.state.count} </strong> ...
        <button onClick={() => this.increase()}>Increate One</button>
        <button onClick={() => this.increaseFive()}>Increate Five</button>
        <hr />
      </div>
    );
  }
}

export default Count;
