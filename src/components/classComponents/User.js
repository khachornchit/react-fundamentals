import React, { Component } from "react";

class User extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoggedIn: true
    };
  }

  render() {
    return (
      this.state.isLoggedIn && (
        <div>Welcome {this.props.name}</div>
      )
    );

    // 2nd approach
    // return this.state.isLoggedIn ? (
    //   <div>Welcome John</div>
    // ) : (
    //   <div>Welcome Guest</div>
    // );
    // 1st approach
    // let message = null;
    // if (this.state.isLoggedIn) {
    //   message = "Welcome John";
    // } else {
    //   message = "Welcome guest";
    // }
    // return (
    //   <div className="Component-margin">
    //     <h2>{message}</h2>
    //   </div>
    // );
  }
}

export default User;
