import React, { Component } from "react";

class BindEventHandler extends Component {
  constructor(props) {
    super(props);

    this.state = {
      message: "Hello, this is a starting message :)"
    };

    // {/* 3rd approach */}
    // this.clickHandler = this.clickHandler.bind(this);
  }

  //   clickHandler() {
  //     this.setState(
  //       {
  //         message: "Updated message successfully !"
  //       },
  //       () => console.log(this.state.message)
  //     );
  //   }

  clickHandler = () => {
    this.setState(
      {
        message: "Updated message using the arrow function successfully !"
      },
      () => console.log(this.state.message)
    );
  };

  render() {
    return (
      <div className="Component-margin">
        <p>
          <em>Message: {this.state.message}</em>
        </p>

        {/* 1st approach */}
        {/* <button onClick={this.clickHandler.bind(this)}>Bind Event Handler</button> */}

        {/* 2nd approach */}
        {/* <button onClick={() => this.clickHandler()}>Bind Event Handler</button> */}

        {/* 3rd approach */}
        {/* <button onClick={this.clickHandler}>Bind Event Handler</button> */}

        {/* 4th approach */}
        <button onClick={this.clickHandler}>Bind Event Handler</button>
      </div>
    );
  }
}

export default BindEventHandler;
