import React, { Component } from "react";

class ClassClick extends Component {
  clickMe() {
    console.log("Click me from a class component.");
  }
  
  render() {
    return (
      <div className="Component-margin">
        <button onClick={this.clickMe}>
          Click me from <strong>a class component</strong>
        </button>
      </div>
    );
  }
}

export default ClassClick;
