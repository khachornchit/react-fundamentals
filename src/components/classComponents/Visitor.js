import React, { Component } from "react";

class Visitor extends Component {
  constructor() {
    super();
    this.state = {
      welcome: "Welcome to our website",
      message: "This is a React website !"
    };
  }

  changeMessage() {
    this.setState({
      message: "Thanks for subscribed !"
    });
  }

  render() {
    const { welcome, message } = this.state;
    return (
      <div className="Component-margin">
        <h1>{welcome}</h1>
        <p>{message}</p>
        <button onClick={() => this.changeMessage()}>Subscribe</button>
      </div>
    );
  }
}

export default Visitor;
