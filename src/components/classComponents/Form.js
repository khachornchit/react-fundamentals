import React, { Component } from "react";
import User from "./User";

class Form extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "",
      message: "",
      topic: "",
      loginName: ""
    };
  }

  handlerOnChangeName = event => {
    this.setState({
      name: event.target.value
    });
  };

  handlerOnChangeMessage = event => {
    this.setState({
      message: event.target.value
    });
  };

  handlerOnChangeTopic = event => {
    this.setState({
      topic: event.target.value
    });
  };

  handlerOnSubmit = event => {
    alert(
      `Welcome ${this.state.name} ${this.state.message}. Your topic is ${this.state.topic}`
    );
    this.setState({
      loginName: this.state.name
    });

    event.preventDefault();
  };

  render() {
    const { name, message, topic, loginName } = this.state;
    return (
      <form className="Component-margin" onSubmit={this.handlerOnSubmit}>
        <label>Name: </label>
        <input
          type="text"
          placeholder="Input your name"
          value={name}
          onChange={this.handlerOnChangeName}
        />
        <div>
          <label>Message: </label>
          <textarea
            placeholder="Input your message"
            value={message}
            onChange={this.handlerOnChangeMessage}
          />
        </div>
        <div>
          <label>Topic: </label>
          <select value={topic} onChange={this.handlerOnChangeTopic}>
            <option value="react">React</option>
            <option value="angular">Angular</option>
            <option value="vue">Vue</option>
          </select>
        </div>
        <button>Login</button>
        <User name={loginName} />
      </form>
    );
  }
}

export default Form;
