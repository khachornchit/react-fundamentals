import React, { Component } from "react";

class Message extends Component {
  render() {
    const {name, children} = this.props;

    return (
      <div className="Component-margin">
        <li>
          Class component: Message from {name}
          <ul className="Bullet-list-none">
            <li>
              <em>{children}</em>
            </li>
          </ul>
        </li>
      </div>
    );
  }
}

export default Message;