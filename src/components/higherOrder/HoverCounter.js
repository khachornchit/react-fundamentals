import React, { Component } from "react";
import WithCounter from "./WithCounter";

class HoverCounter extends Component {
  render() {
    const { count, incCount, name } = this.props;

    return (
      <div>
        <span onMouseOver={incCount}>
          <strong>{name}</strong> have hand-overed {count}
        </span>
      </div>
    );
  }
}

export default WithCounter(HoverCounter, 1);
