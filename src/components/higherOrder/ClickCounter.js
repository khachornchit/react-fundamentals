import React, { Component } from "react";
import WithCounter from "./WithCounter";

class ClickCounter extends Component {
  render() {
    const { count, incCount, name } = this.props;

    return (
      <div>
        <button onClick={incCount}>
          {name} have clicked {count} timex
        </button>
      </div>
    );
  }
}

export default WithCounter(ClickCounter, 5);