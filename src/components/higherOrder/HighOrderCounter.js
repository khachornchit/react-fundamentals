import React, { Component } from "react";
import ClickCounter from "./ClickCounter";
import HoverCounter from "./HoverCounter";

class HighOrderCounter extends Component {
  render() {
    return (
      <div className="Component-margin">
        <hr />
        <p>Higher Order Component (HOC)</p>
        <ClickCounter name="HOC" />
        <HoverCounter name="HOC" />
      </div>
    );
  }
}

export default HighOrderCounter;