import React, { Component } from "react";

const withCounter = (WrappedComponent, increaaseCounter) => {
  class WithCounter extends Component {
    constructor(props) {
      super(props);

      this.state = {
        count: 0
      };
    }

    incCount = () => {
      this.setState(prevState => {
        return { count: prevState.count + increaaseCounter };
      });
    };

    render() {
      return (
        <WrappedComponent
          count={this.state.count}
          incCount={this.incCount}
          {...this.props}
        />
      );
    }
  }

  return WithCounter;
};

export default withCounter;
