import React, { Component } from "react";
import RegularComponentChild from "./RegularComponentChild";
import PureComponentChild from "./PureComponentChid";
import MemoComponentChild from "./MemoComponentChild";

class PureComponentParent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "John"
    };
  }

  componentDidMount() {
    setInterval(() => {
      this.setState({
        name: "John"
      });
    }, 2000);
  }

  render() {
    console.log("Pure component parent render");
    return (
      <div className="Component-margin">
        <MemoComponentChild name={this.state.name} />
        {/* <RegularComponentChild />
        <PureComponentChild /> */}
      </div>
    );
  }
}

export default PureComponentParent;
