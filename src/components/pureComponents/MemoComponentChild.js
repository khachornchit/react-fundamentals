import React from "react";

function MemoComponentChild({ name }) {
  console.log("Memo component render");

  return (
    <>
      <h3>Memo component render {name}</h3>
    </>
  );
}

export default React.memo(MemoComponentChild);