import React, { PureComponent } from "react";

class PureComponentChild extends PureComponent {
  render() {
    console.log("<<< --- Pure component render --- >>>");
    return (
      <div>
        <h3>Pure Component {this.props.name}</h3>
      </div>
    );
  }
}

export default PureComponentChild;
