import React, { Component } from "react";

class RegularComponentChild extends Component {
  render() {
    console.log("Regular component child render");
    return (
      <div>
        <h3>Regulare Component {this.props.name}</h3>
      </div>
    );
  }
}

export default RegularComponentChild;
