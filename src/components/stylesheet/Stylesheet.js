import React from "react";
import "../../css/styles.css";

const Stylesheet = (props) =>  {
  let className = props.class ? "primary" : "";
  return (
    <div className="Component-margin">
      <h1 className={`${className} font-xl`}><em>React stylesheet</em></h1>
    </div>
  );
};

export default Stylesheet;