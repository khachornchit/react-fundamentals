import React from "react";

const heading = {
  color: 'blue',
  fontSize: '25px'
};

const InlineStyle = () => {
  return (
    <div className="Component-margin">
      <h1 style={heading}><em>Stylesheet inline approach</em></h1>
    </div>
  );
};

export default InlineStyle;