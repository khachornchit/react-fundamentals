import React from "react";
import styles from "../../css/styles.module.css";

const StyleSheetModule = () => {
  return (
    <div className="Component-margin">
      <h2 className={styles.success}>
        <em>Stylesheet module approach</em>
      </h2>
    </div>
  );
};

export default StyleSheetModule;