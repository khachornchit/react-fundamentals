import React, { Component } from "react";
import ChildComponent from "./ChildComponent";

class ParentComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "Parent Component"
    };
  }

  parentGreetingHandler = childName => {
    alert(`Hello ${this.state.name}, I come from ${childName}`);
  };

  render() {
    return (
      <div className="Component-margin">
        <p>
          Define function in a parent component then calling the function from a
          child component.
        </p>
        <ChildComponent greetingHandler={this.parentGreetingHandler} />
      </div>
    );
  }
}

export default ParentComponent