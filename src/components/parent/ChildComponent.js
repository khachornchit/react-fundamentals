import React from "react";

const ChildComponent = props => {
  return (
    <div>
      <button onClick={() => props.greetingHandler("Child Component")}>
        I'm a button from a child componenent
      </button>
    </div>
  );
};

export default ChildComponent;