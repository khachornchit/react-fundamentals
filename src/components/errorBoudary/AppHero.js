import React, { Component } from "react";
import ErrorBoundary from "./ErrorBoundary";
import Hero from "./Hero";

class AppHero extends Component {
  render() {
    return (
      <div className="Component-margin">
        <ErrorBoundary>
          <Hero heroName="Batman" />
        </ErrorBoundary>

        <ErrorBoundary>
          <Hero heroName="Superman" />
        </ErrorBoundary>

        <ErrorBoundary>
          <Hero heroName="Joker" />
        </ErrorBoundary>
      </div>
    );
  }
}

export default AppHero;