import React, { Component } from "react";

class LifeCycleUpdate extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "John"
    };

    console.log("Life Cycle Update: Constructor");
  }

  static getDerivedStateFromProps(props, state) {
    console.log("Life Cycle Update: getDerivedStateFromProps");
    return null;
  }

  componentDidMount() {
    console.log("Life Cycle Update: componentDidMount");
  }

  shouldComponentUpdate() {
    console.log("Life Cycle Update: shouldComponentUpdate");
    return true;
  }

  getSnapshotBeforeUpdate(prevProps, prevState) {
    console.log("Life Cycle Update: getSnapshotBeforeUpdate");
    return null;
  }

  componentDidUpdate() {
    console.log("Life Cycle Update: componentDidUpdate");
  }

  updateState = () => {
    this.setState({
      name: "Update name"
    });
  };

  render() {
    console.log("Life Cycle Update: render");
    return (
      <div className="Component-margin">
        Life Cycle Update: render
        <button onClick={this.updateState}>Update State</button>
      </div>
    );
  }
}

export default LifeCycleUpdate;
