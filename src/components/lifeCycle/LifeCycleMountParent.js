import React, { Component } from "react";
import LifeCycleMountChild from "./LifeCycleMountChild";

class LifeCycleMountParent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "John"
    };

    console.log("Life Cycle Mount Parent: Constructor");
  }

  static getDerivedStateFromProps(props, state) {
    console.log("Life Cycle Mount Parent: getDerivedStateFromProps");
    return null;
  }

  componentDidMount() {
    console.log("Life Cycle Mount Parent: componentDidMount");
  }

  shouldComponentUpdate() {
    console.log("Life Cycle Mount Parent: shouldComponentUpdate");
    return true;
  }

  getSnapshotBeforeUpdate(prevProps, prevState) {
    console.log("Life Cycle Mount Parent: getSnapshotBeforeUpdate");
    return null;
  }

  componentDidUpdate() {
    console.log("Life Cycle Mount Parent: componentDidUpdate");
  }

  updateState = () => {
    this.setState({
      name: "Update name"
    });
  };

  render() {
    console.log("Life Cycle Mount Parent: render");
    return (
      <div className="Component-margin">
        <div>Life Cycle Mount Parent: render</div>
        <button onClick={this.updateState}>Update State Parent</button>
        <LifeCycleMountChild />
      </div>
    );
  }
}

export default LifeCycleMountParent;
