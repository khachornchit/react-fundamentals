import React, { Component } from "react";

class LifeCycleMountChild extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "John"
    };

    console.log("Life Cycle Child: Constructor");
  }

  static getDerivedStateFromProps(props, state) {
    console.log("Life Cycle Child: getDerivedStateFromProps");
    return null;
  }

  componentDidMount() {
    console.log("Life Cycle Child: componentDidMount");
  }

  render() {
    console.log("Life Cycle Child: render")
    return <div className="Component-margin">Life Cycle Child: render</div>;
  }
}

export default LifeCycleMountChild;
