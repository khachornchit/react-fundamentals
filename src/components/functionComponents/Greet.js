import React from "react";

// JSX JavaScript ES5
// function Greet() {
//     return <h2>Greet component</h2>;
// }

// Use React.createElement
// const Greet = () => {
//   return React.createElement(
//     "div",
//     { id: "greeting", className: "defaultClass" },
//     React.createElement(
//       "p",
//       null,
//       "Hello, I come React.createElement function !"
//     )
//   );
// };

// JSX JavaScript ES6 Syntax
// className
// htmlFor
// onClick
// tabIndex
// const Greet = () => <p>Greet component using ES6 syntax.</p>

// JSX ES6 syntax and props
// const Greet = props => {
//   return (
//     <div className="Component-margin">
//       <li>
//         Greeting from React. Hello <strong>{props.name}</strong>
//         <ul className="Bullet-list-none">
//           <li>
//             <em>Title : {props.title}</em>
//           </li>
//           <li>
//             <em>{props.children}</em>
//           </li>
//         </ul>
//       </li>
//     </div>
//   );
// };

// JSX and props for the functional component
const Greet = ({ name, title, children }) => {
  return (
    <div className="Component-margin">
      <li>
        Functional component: Greet from <strong>{name}</strong>
        <ul className="Bullet-list-none">
          <li>
            <em>Title : {title}</em>
          </li>
          <li>
            <em>{children}</em>
          </li>
        </ul>
      </li>
    </div>
  );
};

export default Greet;
