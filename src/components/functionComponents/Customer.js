import React from "react";

const Customer = ({customer}) => {
  return (
    <li>
      {customer.name}, email : {customer.email}
    </li>
  );
};

export default Customer;