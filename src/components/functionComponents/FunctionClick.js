import React from "react";

function FunctionClick() {
  function clickMe() {
    console.log("Click me from a function component.");
  }

  return (
    <div className="Component-margin">
      <button onClick={clickMe}>
        Click me from <strong>a function component</strong>
      </button>
    </div>
  );
}

export default FunctionClick;