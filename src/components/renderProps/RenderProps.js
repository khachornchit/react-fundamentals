import React, { Component } from "react";
import HoverCounter from "./HoverCounterRender";
import User from "./User";
import CounterRender from "./CounterRender";
import Counter from "./Counter";

class RenderProps extends Component {
  render() {
    return (
      <div className="Component-margin">
        <hr />
        <p>Render Props</p>

        <User render={isLoggedIn => (isLoggedIn ? "John" : "Guest")} />

        <Counter
          render={(count, incrementCount) => (
            <CounterRender count={count} incrementCount={incrementCount} />
          )}
        />

        <Counter
          render={(count, incrementCount) => (
            <HoverCounter count={count} incrementCount={incrementCount} />
          )}
        />
      </div>
    );
  }
}

export default RenderProps;
