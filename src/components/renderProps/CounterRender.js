import React, { Component } from "react";

class CounterRender extends Component {
  render() {
    const { count, incrementCount } = this.props;

    return (
      <>
        <button onClick={incrementCount}>Increment Count {count} times</button>
      </>
    );
  }
}

export default CounterRender;