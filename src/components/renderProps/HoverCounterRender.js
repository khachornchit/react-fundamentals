import React, { Component } from "react";

class HoverCounter extends Component {
  render() {
    const { count, incrementCount } = this.props;

    return (
      <div>
        <span onMouseOver={incrementCount}>
          You have hand-overed {count}
        </span>
      </div>
    );
  }
}

export default HoverCounter;
