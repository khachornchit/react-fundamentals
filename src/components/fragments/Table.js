import React from "react";
import "../../css/styles.css";
import Column from "./Column";

const Table = () => {
  const items = [
    {
      company: "Alfreds Futterkiste",
      contact: "Maria Anders",
      country: "Germany"
    },
    {
      company: "Centro comercial Moctezuma",
      contact: "Francisco Chang",
      country: "Mexico"
    },
    {
      company: "Ernst Handel",
      contact: "Roland Mendel",
      country: "Austria"
    },
    {
      company: "Island Trading",
      contact: "Helen Bennett",
      country: "UK"
    },
    {
      company: "Laughing Bacchus Winecellars",
      contact: "Yoshi Tannamuri",
      country: "Canada"
    },
    {
      company: "Magazzini Alimentari Riuniti",
      contact: "Giovanni Rovelli",
      country: "Italy"
    }
  ];

  const columns = items.map(item => (
    <Column
      key={item.company}
      company={item.company}
      contact={item.contact}
      country={item.country}
    />
  ));

  return (
    <table className="Component-margin">
      <thead>
        <tr>
          <th>Company</th>
          <th>Contact</th>
          <th>Country</th>
        </tr>
        {columns}
      </thead>
      <tbody></tbody>
    </table>
  );
};

export default Table;
