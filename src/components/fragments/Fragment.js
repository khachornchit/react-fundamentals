import React from 'react'

const Fragment = () => {
    return (
        <React.Fragment>
            <h3>React Fragment</h3>
            <p>Use React.Fragment instead of the single div in return function</p>
        </React.Fragment>
    )
}

export default Fragment