import React from "react";

function Column({ company, contact, country }) {
  return (
    <>
      <tr>
        <td>{company}</td>
        <td>{contact}</td>
        <td>{country}</td>
      </tr>
    </>
  );
}

export default Column;