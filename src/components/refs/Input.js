import React, { Component } from "react";

class Input extends Component {
  constructor(props) {
    super(props);
    this.inputRef = React.createRef();
  }

  focusInput = () => {
    this.inputRef.current.focus();
    alert("Hello, I'm be focused !");
  };

  render() {
    return (
      <div>
        <input type="text" placeholder="Input some text" ref={this.inputRef} />
      </div>
    );
  }
}

export default Input;