import React, { Component } from "react";
import Input from "./Input";

class InputParent extends Component {
  constructor(props) {
    super(props);
    this.componentRef = React.createRef();
  }

  handlerClickMe = () => {
    this.componentRef.current.focusInput();
  };

  render() {
    return (
      <div className="Component-margin">
        Input parent for componet refs testing
        <Input ref={this.componentRef} />
        <button onClick={this.handlerClickMe}>Click me</button>
      </div>
    );
  }
}

export default InputParent;
