import React, { Component } from "react";

class RefsDemo extends Component {
  constructor(props) {
    super(props);
    this.inputRef = React.createRef();
  }

  componentDidMount() {
    this.inputRef.current.focus();
    console.log(this.inputRef);
  }

  handlerClick = () => {
    alert(this.inputRef.current.value);
  };

  render() {
    return (
      <div className="Component-margin">
        <input type="text" ref={this.inputRef} placeholder="Input some value" />
        <button onClick={this.handlerClick}>Click me</button>
      </div>
    );
  }
}

export default RefsDemo;
