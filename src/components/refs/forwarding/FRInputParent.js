import React, { Component } from "react";
import FRInput from "./FRInput";

class FRInputParent extends Component {
  constructor(props) {
    super(props);
    this.frInputRef = React.createRef();
  }

  handleClick = () => {
    this.frInputRef.current.focus();
    this.frInputRef.current.value = "Hello";
  };

  render() {
    return (
      <div className="Component-margin">
        <hr />
        Forwarding Input Parent
        <FRInput ref={this.frInputRef} />
        <button onClick={this.handleClick}>Forwarding</button>
      </div>
    );
  }
}

export default FRInputParent;
